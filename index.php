<?php
  require_once "db_credentials.php";

	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}

	$form_name = $form_comment = "";
	$msg = "";

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$form_name = $_POST['form_name'];
		$form_comment = $_POST['form_comment'];

		$name = mysqli_real_escape_string($conn, $form_name);
		$comment = mysqli_real_escape_string($conn, $form_comment);

		$sql = "INSERT INTO $table (nome, comentario)
			VALUES ('$name', '$comment')";

		if (!mysqli_query($conn, $sql)) {
			die("Error: " . $sql . "<br>" . mysqli_error($conn));
		}
		else {
			$form_name = $form_comment = "";
			$msg = "Comentário salvo com sucesso!";
		}
	}

	$sql = "SELECT * FROM $table";
	$comments = mysqli_query($conn, $sql);

	if (!$comments) {
		die("Error: " . $sql . "<br>" . mysqli_error($conn));
	}

	mysqli_close($conn);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Artigo</title>
	</head>
	<body>
		<h1>Título do artigo</h1>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nulla est, dapibus vitae augue a, euismod pretium quam. Integer quis libero eget orci vestibulum facilisis. Praesent placerat arcu sapien, vel laoreet eros sodales iaculis. Duis ultricies pulvinar sollicitudin. Morbi laoreet, dui eu placerat volutpat, dui lectus porttitor lacus, non commodo ligula enim a nibh. Sed aliquet porta augue sed laoreet. Fusce nec erat est. Nullam mattis quis ipsum in rhoncus. Etiam a tellus elit. In dui orci, interdum in dui ut, accumsan tristique velit. Aenean consectetur arcu id risus rutrum, non eleifend massa vehicula. Suspendisse potenti. Donec dapibus ut est consectetur luctus. Etiam convallis, tellus at scelerisque convallis, nunc felis semper metus, sit amet luctus sapien metus non erat. Sed elementum tellus maximus purus placerat, sit amet scelerisque nibh scelerisque.
		</p>

		<hr>
		<div class="comments">
			<h2>Comentários</h2>

			<?php if (!empty($msg)): ?>
				<?= $msg ?>
			<?php endif; ?>

			<?php if (mysqli_num_rows($comments) > 0): ?>
				<?php while($comment = mysqli_fetch_assoc($comments)): ?>
					<div class="comment" id="comment_<?= $comment['id'] ?>">
						<h4>De: <?= $comment['nome'] ?></h4>
						<p><?= $comment['comentario'] ?></p>
					</div>
				<?php endWhile; ?>
			<?php else: ?>
				Nenhum comentário enviado.
			<?php endIF; ?>

			<hr>
			<h3>Novo comentário</h3>
			<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
				Nome:<br>
				<input type="text" name="form_name" value="<?= $form_name ?>" placeholder="Seu nome"><br>
				Comentário:<br>
				<textarea name="form_comment" rows="8" cols="80" placeholder="Seu comentário"><?= $form_comment ?></textarea><br>
				<input type="submit" name="submit" value="Enviar">
			</form>
		</div>
	</body>
</html>
